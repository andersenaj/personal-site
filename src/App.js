import './App.css';
import About from './Components/About';
import Contact from './Components/Contact';
import Experience from './Components/Experience';
import Header from './Components/Header';
import Technologies from './Components/Technologies';

function App() {
  return (
    <div className="App">
      <Header/>
      <About/>
      <Contact/>
      <Experience/>
      <Technologies/>
    </div>
  );
}

export default App;
